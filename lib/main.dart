import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          foregroundColor: Colors.white,
          backgroundColor: Colors.black,
          title: Text('ANGEL'),
        ),
        body: Center(
          child: Container(
            child: MyN(),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add_box),
          backgroundColor: Colors.cyan,
          elevation: 20.0,
          onPressed: () {
            print("Presionado");
          },
        ),
      ),
    );
  }
}


class MyN extends StatelessWidget {
  const MyN({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () => showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Animes'),
          content: const Text('Ver la'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ],
        ),
      ),
      child: const Text('hELLO WORD'),
    );
  }
}
